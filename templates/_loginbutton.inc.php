<style>
    #oauth2-google-signin-button.persona-button span::after {
        background-image : url('<?= image_url('g-normal.png', false, 'oauth2_google'); ?>');
    }
</style>
<div class="logindiv openid_container" id="oauth2_google_openid_container" style="display: block;">
    <div style="text-align: center;">
        <a class="persona-button large blue" id="oauth2-google-signin-button" href="<?= make_url('oauth2_google_login'); ?>"><span><?php echo __('Sign in with Google'); ?></span></a>
    </div>
</div>